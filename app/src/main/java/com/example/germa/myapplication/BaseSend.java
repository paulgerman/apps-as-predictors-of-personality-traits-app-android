package com.example.germa.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


abstract class BaseSend
{
    public static final String TAG = "MIAU";
    //private static final String SERVER_URL = "http://192.168.2.153/publicEndpoint";
    //public static final String QUIZ_URL = "http://192.168.2.153/demo/quiz.php";
    public static final String SERVER_URL = "http://cypien.com/app/publicEndpoint";
    public static final String QUIZ_URL = "http://cypien.com/app";

    public static final String VERSION = "4";

    public static void sendPost(String url, final JSONObject parameters, final InterfaceSend baseSend, final Activity activity)
    {
        Log.v(TAG, url);
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(activity);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.v(TAG, response);
                        try
                        {
                            JSONObject responseJson = new JSONObject(response);
                            Log.v(TAG, "CODE " + responseJson.getString("result"));
                            baseSend.setCode(responseJson.getString("result"));
                            SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString("CODE", responseJson.getString("result"));
                            editor.commit();

                            Toast toast = Toast.makeText(activity, "Thank you!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.v(TAG, "That didn't work!" + error.toString());
                Toast toast = Toast.makeText(activity, "ERROR!!! Do you have internet?", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", parameters.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        // Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
}
