package com.example.germa.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SendData extends AppCompatActivity implements InterfaceSend
{

    Context context;

    private String code;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_data);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        Button b = (Button)findViewById(R.id.button2);
        b.setEnabled(false);
        TextView t = (TextView)findViewById(R.id.textView9);

        t.setText(Html.fromHtml("Visit webpage <strong>" + BaseSend.QUIZ_URL + "</strong> and enter code"));

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String codeTmp = sharedPref.getString("CODE", "nope");
        if(!codeTmp.equals("nope"))
        {
            this.setCode(codeTmp);
        }
        else
        {
            try
            {
                sendData(SendData.getAppsString(getPackageManager()));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static String getAppsString(PackageManager pm)
    {
        List<ApplicationInfo> apps = pm.getInstalledApplications(0);
        ArrayList<String> appNames = new ArrayList<>();
        for (ApplicationInfo info : apps)
        {
            if((info.flags & ApplicationInfo.FLAG_SYSTEM) == 0)
                appNames.add(info.packageName);
        }
        String msg;
        try
        {
           return new JSONArray(appNames).toString();
        }
        catch (Exception exc)
        {
            Log.v(BaseSend.TAG, "nuuuuuuuuu");
            return null;
        }
    }

    private void sendData(String appsJson) throws JSONException
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("method", "survey_create");
        JSONArray params = new JSONArray();
        JSONObject surveyObject = new JSONObject();
        surveyObject.accumulate("app_survey_content", appsJson);
        surveyObject.accumulate("app_survey_type", "learning");
        surveyObject.accumulate("app_master_version", BaseSend.VERSION);
        params.put(surveyObject);
        jsonObject.accumulate("params", params);

        BaseSend.sendPost(BaseSend.SERVER_URL, jsonObject, this, this);
    }

    public void setCode(String code)
    {
        this.code = code;
        TextView t = (TextView)findViewById(R.id.textView10);
        t.setText(this.code);
        Button b = (Button)findViewById(R.id.button2);
        b.setEnabled(true);
    }

    public void startWebView(View view)
    {
        Intent intent = new Intent(this, Webview.class);
        intent.putExtra("CODE", this.code);
        startActivity(intent);
    }


}
