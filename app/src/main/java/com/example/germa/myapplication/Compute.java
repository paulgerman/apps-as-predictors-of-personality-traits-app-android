package com.example.germa.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Compute extends AppCompatActivity implements InterfaceSend
{
    private String code;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compute);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Button b = (Button)findViewById(R.id.button4);
        b.setEnabled(false);
        b = (Button)findViewById(R.id.button5);
        b.setEnabled(false);

        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        String codeTmp = sharedPref.getString("CODE", "nope");
        if(!codeTmp.equals("nope"))
        {
            this.setCode(codeTmp);
        }
        else
        {
            try
            {
                sendData(SendData.getAppsString(getPackageManager()));
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void setCode(String code)
    {
        this.code = code;
        TextView t = (TextView)findViewById(R.id.textView7);
        t.setText(this.code);
        Button b = (Button)findViewById(R.id.button4);
        b.setEnabled(true);
        b = (Button)findViewById(R.id.button5);
        b.setEnabled(true);
    }

    private void sendData(String appsJson) throws JSONException
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("method", "survey_create");
        JSONArray params = new JSONArray();
        JSONObject surveyObject = new JSONObject();
        surveyObject.accumulate("app_survey_content", appsJson);
        surveyObject.accumulate("app_survey_type", "AI");
        surveyObject.accumulate("app_master_version", BaseSend.VERSION);
        params.put(surveyObject);
        jsonObject.accumulate("params", params);


        BaseSend.sendPost(BaseSend.SERVER_URL, jsonObject, this, this);
    }

    public void startWebView(View view)
    {
        Intent intent = new Intent(this, Webview.class);
        intent.putExtra("CODE", this.code);
        startActivity(intent);
    }

    public void recalculate(View view)
    {
        Button b = (Button)findViewById(R.id.button5);
        b.setEnabled(false);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = BaseSend.SERVER_URL+"?q={\"method\":\"survey_process\",\"params\":[\"" + this.code + "\"]}";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v(BaseSend.TAG, "Recalculated");
                        Toast toast = Toast.makeText(Compute.this, "Recalculated!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        Button b = (Button)findViewById(R.id.button5);
                        b.setEnabled(true);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v(BaseSend.TAG, "Recalculate didn't work!" + error.toString());
                Toast toast = Toast.makeText(Compute.this, "ERROR!!! Do you have internet?", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                Button b = (Button)findViewById(R.id.button5);
                b.setEnabled(true);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
}
